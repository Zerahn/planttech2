package net.kaneka.planttech2.entities;

public interface IAffectPlayerRadiation
{
    float getAmount();
}
